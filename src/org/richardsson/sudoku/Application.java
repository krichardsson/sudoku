package org.richardsson.sudoku;

import java.util.LinkedList;
import java.util.List;

public class Application {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("*** Sudoku solver ***");		
		Application tApplication = new Application();
	    tApplication.solve();		
	}

	private Board mBoard = new Board(mHard);
    private List<Solver> mSolvers = new LinkedList<Solver>();

	
	private void solve() {
		createStandardSolvers();
		
		mBoard.print();
		
		int lastFillCount = 0;
		int tCount = 0;
		while(!mBoard.isFilled() && (mBoard.getFillCount() > lastFillCount)) {
			lastFillCount = mBoard.getFillCount();

			System.out.println("*** Start iteration " + (tCount + 1) + " ***");
			
			WorkBoard tWorkBoard = new WorkBoard();
			reduceWorkBoard(tWorkBoard);
			System.out.println("Work board after reductions");
			tWorkBoard.print();
			
			System.out.println("*** Merge ***");
			
			Board tPreMergeBoard = new Board(mBoard);			
			MergeBoards(tWorkBoard, mBoard);
			tPreMergeBoard.printDiff(mBoard);
			mBoard.print();
			
			System.out.println("*** End iteration " + (tCount + 1) + " ***");
			
			tCount++;
		}
	}
	
	
	private void MergeBoards(WorkBoard pWorkBoard, Board pBoard) {
		for(int tRow = 0; tRow < 9; tRow++) {
			for(int tCollumn = 0; tCollumn < 9; tCollumn++) {
				if(pWorkBoard.isSingle(tRow, tCollumn)) {
					int tValue = pWorkBoard.getPointSingleValue(tRow, tCollumn);
					
					if(pBoard.hasValue(tRow, tCollumn)) {
						assert(pBoard.getValue(tRow, tCollumn) == tValue);
					}
					
					pBoard.setValue(tRow, tCollumn, tValue);
				}
			}
		}
	}


	private void reduceWorkBoard(WorkBoard pWorkBoard) {
		final boolean tPrint = false;
		
		for(Solver tSolver : mSolvers) {
			WorkBoard tPreWorkBoard = null;
			if(tPrint) {
				tPreWorkBoard = new WorkBoard(pWorkBoard);
			}
			
			tSolver.reduce(mBoard, pWorkBoard);
			
			if(tPrint) {
				System.out.println("Reduce with solver " + tSolver.print());
				tPreWorkBoard.printDiff(pWorkBoard);
			}
		}
	}


	private void createStandardSolvers() {
		// Board solvers
		mSolvers.add(new InitialSolver());
		
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++){
				mSolvers.add(new BoxBoardSolver(i, j));
			}
		}
		
		for(int i=0; i<9; i++) {
			mSolvers.add(new RowBoardSolver(i));
		}

		for(int i=0; i<9; i++) {
			mSolvers.add(new CollumnBoardSolver(i));
		}
		
		// Workboard solvers
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++){
				mSolvers.add(new BoxWorkSolver(i, j));
			}
		}
		
		for(int i=0; i<9; i++) {
			mSolvers.add(new RowWorkSolver(i));
		}

		for(int i=0; i<9; i++) {
			mSolvers.add(new CollumnWorkSolver(i));
		}
		
	}


	private static String[] mSimple = {
		"892" + "46 " + "   ",
		" 7 " + " 9 " + "46 ",
		"64 " + " 5 " + " 82",
		
		"   " + " 37" + "  5",
		"538" + "  4" + "  9",
		"  6" + "   " + " 34",

		"35 " + "94 " + "1  ",
		" 69" + "81 " + "34 ",
		"   " + "3  " + "59 "};

	private static String[] mSimple2 = {
		"892" + "46 " + "   ",
		" 7 " + " 9 " + "46 ",
		"64 " + " 5 " + " 82",
		
		"   " + " 37" + "  5",
		"538" + "  4" + "  9",
		"  6" + "   " + " 34",
	
		"35 " + "94 " + "1  ",
		" 69" + "81 " + "34 ",
		"   " + "3  " + "59 "};

	private static String[] mHard = {
		"   " + "6 4" + "   ",
		"  2" + "7  " + "   ",
		"  9" + "2  " + " 58",
		
		"4  " + "  1" + "   ",
		" 9 " + "   " + " 3 ",
		"51 " + "   " + "4  ",
	
		"2  " + " 8 " + "  5",
		" 5 " + " 6 " + " 9 ",
		"   " + "   " + "27 "};

	private static String[] mSuperHard = {
		"  6" + "   " + " 9 ",
		"   " + "38 " + " 2 ",
		"1 7" + "   " + "5  ",
		
		"  1" + "2 7" + "9  ",
		"94 " + "   " + "7  ",
		"   " + " 3 " + " 6 ",
	
		"2  " + "9  " + "   ",
		" 5 " + "   " + "  8",
		"   " + "  1" + "   "};
}
