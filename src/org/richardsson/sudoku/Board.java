package org.richardsson.sudoku;


public class Board {
	private Value[][] mValues = new Value[9][9];

	public Board() {
		Value tNoValue = new Value();
		
		for(int y = 0; y < 9; y++) {
			for(int x = 0; x < 9; x++) {
				mValues[y][x] = tNoValue;
			}
		}
	}
	
	public Board(String[] pInitialData) {
		fillBoard(pInitialData);
	}
	
	public Board(Board pBoard) {
		for(int y = 0; y < 9; y++) {
			for(int x = 0; x < 9; x++) {
				mValues[y][x] = pBoard.mValues[y][x];
			}
		}
	}

	public void print() {
		Line.printSpacer();

		int tRowSpacer = 0;
		for(int tRow = 0; tRow < 9; tRow++) {
			Line tLine = getRow(tRow);
			tLine.print();

			tRowSpacer++;
			if(tRowSpacer == 3) {
				tRowSpacer = 0;
				Line.printSpacer();
			}
		}
	}

	public void printDiff(Board pSecondBoard) {
		Line.printSpacer();

		int tRowSpacer = 0;
		for(int tRow = 0; tRow < 9; tRow++) {
			Line tFirstLine = getRow(tRow);
			Line tSecondLine = pSecondBoard.getRow(tRow);
			Line tDiffLine = new Line();
			
			for(int i = 0; i < 9; i++) {
				if( !tFirstLine.getValue(i).equals(tSecondLine.getValue(i)) ){
					tDiffLine.setValue(i, tSecondLine.getValue(i));
				}
			}
			
			tDiffLine.print();

			tRowSpacer++;
			if(tRowSpacer == 3) {
				tRowSpacer = 0;
				Line.printSpacer();
			}
		}
	}
	
	public void setValue(int pRow, int pCollumn, int pValue) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		
		mValues[pRow][pCollumn] = new Value(pValue);
	}

	public void setNoValue(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		
		mValues[pRow][pCollumn] = new Value();
	}
	
	public boolean hasValue(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		
		return mValues[pRow][pCollumn].hasValue();
	}
	
	public int getValue(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		assert(mValues[pRow][pCollumn].hasValue());
		
		return mValues[pRow][pCollumn].getValue();
	}
	
	public Line getRow(int pRow) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		
		Line tLine = new Line();
		for(int i = 0; i < 9; i++) {
			tLine.setValue(i, mValues[pRow][i]);
		}
		
		return tLine;
	}

	public Line getCollumn(int pCollumn) {
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		
		Line tLine = new Line();
		for(int i = 0; i < 9; i++) {
			tLine.setValue(i, mValues[i][pCollumn]);
		}
		
		return tLine;		
	}
	
	public Box getBox(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 2);
		assert(pCollumn >= 0);
		assert(pCollumn <= 2);
		
		Box tBox = new Box();
		for(int tRow = 0; tRow < 3; tRow++) {
			for(int tCollumn = 0; tCollumn < 3; tCollumn++) {
				tBox.setValue(tRow, tCollumn, mValues[tRow + pRow * 3][tCollumn + pCollumn * 3]);
			}
		}
		
		return tBox;		
	}
	
	public boolean validate() {
		return true;
		// TODO
	}

	public int getFillCount() {
		int tCount = 0;
		
		for(int tRow = 0; tRow < 9; tRow++) {
			for(int tCollumn = 0; tCollumn < 9; tCollumn++) {
				if(mValues[tRow][tCollumn].hasValue()) {
					tCount++;
				}
			}			
		}
		
		return tCount;
	}
	
	public boolean isFilled() {
		return (getFillCount() == 81);
	}
	
	private void fillBoard(String[] mRowStrings) {
		for(int y = 0; y < 9; y++) {
			String tRow = mRowStrings[y];
			
			for(int x = 0; x < 9; x++) {
				mValues[y][x] = new Value();
				char tValue = tRow.charAt(x);
				
				if(tValue == ' ') {
					setNoValue(y, x);
				}
				else
				{
					int tValueInt = tValue - '0';
					setValue(y, x, tValueInt);
				}				
			}
		}
	}


}
