package org.richardsson.sudoku;

public class Box extends Group {

	public Value getValue(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 2);
		assert(pCollumn >= 0);
		assert(pCollumn <= 2);
		
		return mValues[pCollumn + pRow * 3];
	}

	public void setValue(int pRow, int pCollumn, Value pValue) {
		assert(pRow >= 0);
		assert(pRow <= 2);
		assert(pCollumn >= 0);
		assert(pCollumn <= 2);
		
		mValues[pCollumn + pRow * 3] = pValue;
	}

	public void setValue(int pRow, int pCollumn, int pValue) {
		setValue(pRow, pCollumn, new Value(pValue));
	}
	
	public static void printSpacer() {
		System.out.println("+-------+");
	}
	
	@Override
	public void print() {		
		for(int tRow = 0; tRow < 3; tRow++) {			
			String tRowString = "";

			for(int tCollumn = 0; tCollumn < 3; tCollumn++) {
				Value tValue = getValue(tRow, tCollumn);
				
				if(tValue.hasValue()) {
					tRowString += tValue.getValue();
				}
				else {
					tRowString += " ";
				}		
				
				tRowString += " ";
			}

			System.out.println("| " + tRowString + "|");
		}		
	}

}
