package org.richardsson.sudoku;

public class BoxBoardSolver extends Solver {

	private int mRow;
	private int mCollumn;

	public BoxBoardSolver(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 2);
		assert(pCollumn >= 0);
		assert(pCollumn <= 2);
		
		mRow = pRow;
		mCollumn = pCollumn;		
	}

	@Override
	public String print() {
		return "BoxBoardSolver for row " + (mRow + 1) + ", collumn " + (mCollumn + 1);
	}

	/**
	 * Find all numbers in the box. Reduce these numbers for all positions in the box. 
	 */
	@Override
	public void reduce(Board pBoard, WorkBoard pWorkBoard) {
		final int tBaseRow = mRow * 3;
		final int tBaseCollumn = mCollumn * 3;
		
		for(int tRow = tBaseRow; tRow < (tBaseRow + 3); tRow++) {
			for(int tCollumn = tBaseCollumn; tCollumn < (tBaseCollumn + 3); tCollumn++) {
				if(pBoard.hasValue(tRow, tCollumn)) {
					int tValue = pBoard.getValue(tRow, tCollumn);
				
					removeInBox(pWorkBoard, tValue);
				}
			}
		}
	}

	private void removeInBox(WorkBoard pWorkBoard, int pValue) {
		final int tBaseRow = mRow * 3;
		final int tBaseCollumn = mCollumn * 3;

		for(int tRow = tBaseRow; tRow < (tBaseRow + 3); tRow++) {
			for(int tCollumn = tBaseCollumn; tCollumn < (tBaseCollumn + 3); tCollumn++) {
				pWorkBoard.remove(tRow, tCollumn, pValue);
			}
		}
	}
}
