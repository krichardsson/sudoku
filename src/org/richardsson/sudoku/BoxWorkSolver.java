package org.richardsson.sudoku;

public class BoxWorkSolver extends Solver {

	private int mRow;
	private int mCollumn;

	public BoxWorkSolver(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 2);
		assert(pCollumn >= 0);
		assert(pCollumn <= 2);
		
		mRow = pRow;
		mCollumn = pCollumn;		
	}

	@Override
	public String print() {
		return "BoxWorkSolver for row " + (mRow + 1) + ", collumn " + (mCollumn + 1);
	}

	/**
	 * Find out if there is any number that only exists in one position in the work board. If there is reduce all other number from that position.  
	 */
	@Override
	public void reduce(Board pBoard, WorkBoard pWorkBoard) {
		final int tBaseRow = mRow * 3;
		final int tBaseCollumn = mCollumn * 3;

		int[] tCount = new int[9];
		int[] tPosRow = new int[9];
		int[] tPosCollumn = new int[9];
		
		for(int tRow = tBaseRow; tRow < (tBaseRow + 3); tRow++) {
			for(int tCollumn = tBaseCollumn; tCollumn < (tBaseCollumn + 3); tCollumn++) {
				for(int tIndex = 0; tIndex < 9; tIndex++) {
					int tValue = tIndex + 1;
					if(pWorkBoard.isValueAvalable(tRow, tCollumn, tValue)) {
						tCount[tIndex]++;
						tPosRow[tIndex] = tRow;
						tPosCollumn[tIndex] = tCollumn;						
					}
				}
			}
		}
		
		for(int tIndex = 0; tIndex < 9; tIndex++) {
			if(tCount[tIndex] == 1) {
				int tValue = tIndex + 1;
				pWorkBoard.removeAllBut(tPosRow[tIndex], tPosCollumn[tIndex], tValue);
			}
		}		
	}

}
