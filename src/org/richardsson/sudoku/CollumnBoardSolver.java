package org.richardsson.sudoku;

public class CollumnBoardSolver extends Solver {

	private int mCollumn;

	public CollumnBoardSolver(int pCollumn) {
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		
		mCollumn = pCollumn;
	}
	
	@Override
	public String print() {
		return "CollumnBoardSolver for collumn " + (mCollumn + 1);
	}

	/**
	 * Find all numbers in the collumn. Reduce these numbers for all positions in the collumn. 
	 */	
	@Override
	public void reduce(Board pBoard, WorkBoard pWorkBoard) {
		for(int tRow = 0; tRow < 9; tRow++) {
			if(pBoard.hasValue(tRow, mCollumn)) {
				int tValue = pBoard.getValue(tRow, mCollumn);
				
				for(int i = 0; i < 9; i++) {
					pWorkBoard.remove(i, mCollumn, tValue);
				}
			}
		}
	}

}
