package org.richardsson.sudoku;

public class CollumnWorkSolver extends Solver {

	private int mCollumn;

	public CollumnWorkSolver(int pCollumn) {
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		
		mCollumn = pCollumn;
	}
	
	@Override
	public String print() {
		return "CollumnWorkSolver for collumn " + (mCollumn + 1);
	}

	@Override
	public void reduce(Board pBoard, WorkBoard pWorkBoard) {
		int[] tCount = new int[9];
		int[] tPosRow = new int[9];
		
		for(int tRow = 0; tRow < 9; tRow++) {
			for(int tIndex = 0; tIndex < 9; tIndex++) {
				int tValue = tIndex + 1;
				if(pWorkBoard.isValueAvalable(tRow, mCollumn, tValue)) {
					tCount[tIndex]++;
					tPosRow[tIndex] = tRow;						
				}
			}
		}
		
		for(int tIndex = 0; tIndex < 9; tIndex++) {
			if(tCount[tIndex] == 1) {
				int tValue = tIndex + 1;
				pWorkBoard.removeAllBut(tPosRow[tIndex], mCollumn, tValue);
			}
		}
	}

}
