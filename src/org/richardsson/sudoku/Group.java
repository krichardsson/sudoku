package org.richardsson.sudoku;

public abstract class Group {
	protected Value[] mValues = new Value[9];
	
	public Group() {
		for(int i = 0; i < 9; i++) {
			mValues[i] = new Value();
		}
	}
	
	public abstract void print();
}
