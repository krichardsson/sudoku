package org.richardsson.sudoku;

public class InitialSolver extends Solver {

	@Override
	public String print() {
		return "InitialSolver";
	}

	/**
	 * Remove all workboard numbers on positions that contains a number.
	 */
	@Override
	public void reduce(Board pBoard, WorkBoard pWorkBoard) {
		for(int tRow = 0; tRow < 9; tRow++) {
			for( int tCollumn = 0; tCollumn < 9; tCollumn++) {
				if(pBoard.hasValue(tRow, tCollumn)) {
					pWorkBoard.removeAll(tRow, tCollumn);
				}
			}
		}
	}

}
