package org.richardsson.sudoku;

public class Line extends Group {
	
	public Value getValue(int pIndex) {
		assert(pIndex >= 0);
		assert(pIndex <= 8);
		
		return mValues[pIndex];
	}

	public void setValue(int pIndex, Value pValue) {
		assert(pIndex >= 0);
		assert(pIndex <= 8);
		
		mValues[pIndex] = pValue;
	}

	public static void printSpacer() {
		System.out.println("+-------+-------+-------+ ");
	}
		
	@Override
	public void print() {
		String tRowString = "";
		
		int tCollumnSpacer = 0;
		for(Value tCollumn : mValues) {
			if(tCollumn.hasValue()) {
				tRowString += tCollumn.getValue();
			}
			else {
				tRowString += " ";
			}		
			
			tRowString += " ";
			
			tCollumnSpacer++;
			if(tCollumnSpacer == 3) {
				tCollumnSpacer = 0;
				tRowString += "| ";
			}
		}
		
		System.out.println("| " + tRowString + "");
	}
}
