package org.richardsson.sudoku;

public class RowBoardSolver extends Solver {

	private int mRow;
	
	public RowBoardSolver(int pRow) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		
		mRow = pRow;
	}
	
	@Override
	public String print() {
		return "RowBoardSolver for row " + (mRow + 1);
	}

	/**
	 * Find all numbers in the row. Reduce these numbers for all positions in the row. 
	 */
	@Override
	public void reduce(Board pBoard, WorkBoard pWorkBoard) {
		for(int tCollumn = 0; tCollumn < 9; tCollumn++) {
			if(pBoard.hasValue(mRow, tCollumn)) {
				int tValue = pBoard.getValue(mRow, tCollumn);
				
				for(int i = 0; i < 9; i++) {
					pWorkBoard.remove(mRow, i, tValue);
				}
			}
		}
	}

}
