package org.richardsson.sudoku;

public class RowWorkSolver extends Solver {

	private int mRow;
	
	public RowWorkSolver(int pRow) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		
		mRow = pRow;
	}
	
	
	@Override
	public String print() {
		return "RowWorkSolver for row " + (mRow + 1);
	}

	@Override
	public void reduce(Board pBoard, WorkBoard pWorkBoard) {
		int[] tCount = new int[9];
		int[] tPosCollumn = new int[9];
		
		for(int tCollumn = 0; tCollumn < 9; tCollumn++) {
			for(int tIndex = 0; tIndex < 9; tIndex++) {
				int tValue = tIndex + 1;
				if(pWorkBoard.isValueAvalable(mRow, tCollumn, tValue)) {
					tCount[tIndex]++;
					tPosCollumn[tIndex] = tCollumn;						
				}
			}
		}
		
		for(int tIndex = 0; tIndex < 9; tIndex++) {
			if(tCount[tIndex] == 1) {
				int tValue = tIndex + 1;
				pWorkBoard.removeAllBut(mRow, tPosCollumn[tIndex], tValue);
			}
		}
	}

}
