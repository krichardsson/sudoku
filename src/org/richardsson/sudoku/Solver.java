package org.richardsson.sudoku;

public abstract class Solver {
	public abstract void reduce(Board pBoard, WorkBoard pWorkBoard);
	public abstract String print();
}
