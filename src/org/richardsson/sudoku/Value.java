package org.richardsson.sudoku;

public class Value {
	private int mValue;
	private boolean mHasValue;
	
	public Value() {		
		mHasValue = false;
	}
	
	public Value(int pValue) {
		assert(pValue > 0);
		assert(pValue < 10);
		mValue = pValue;
		mHasValue = true;
	}
	
	public boolean hasValue() {
		return mHasValue;
	}
	
	public int getValue() {
		assert(mHasValue);
		return mValue;
	}
	
	@Override
	public boolean equals(Object tObj) {
		if(tObj.getClass() != this.getClass()) {
			return false;
		}
		
		Value tValue = (Value)tObj;
		if(tValue.mHasValue != mHasValue) {
			return false;
		}

		if(tValue.mValue != mValue) {
			return false;
		}
		
		return true;
	}
}
