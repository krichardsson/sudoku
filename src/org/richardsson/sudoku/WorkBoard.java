package org.richardsson.sudoku;

public class WorkBoard {
	private boolean[][][] mValues = new boolean[9][9][9];

	public WorkBoard() {
		for(int y = 0; y < 9; y++) {
			for(int x = 0; x < 9; x++) {
				for(int i = 0; i < 9; i++) {
					mValues[y][x][i] = true;
				}
			}
		}
	}
		
	public WorkBoard(WorkBoard pWorkBoard) {
		for(int y = 0; y < 9; y++) {
			for(int x = 0; x < 9; x++) {
				for(int i = 0; i < 9; i++) {
					mValues[y][x][i] = pWorkBoard.mValues[y][x][i];
				}
			}
		}
	}

	public void print() {
		printLineSeparator();
		
		int tRowSpacer = 0;
		for(int tRow = 0; tRow < 9; tRow++) {
			boolean[][] tLine = new boolean[9][9]; 
			
			for(int tCol = 0; tCol < 9; tCol++) {
				tLine[tCol] = getPoint(tRow, tCol);
			}

			printLine(tLine);

			tRowSpacer++;
			if(tRowSpacer == 3) {
				tRowSpacer = 0;
				printLineSeparator();
			}
			else {
				printSpacer();
			}
		}
	}

	public void printDiff(WorkBoard pSecondWorkBoard) {
		printLineSeparator();
		
		int tRowSpacer = 0;
		for(int tRow = 0; tRow < 9; tRow++) {
			boolean[][] tLine = new boolean[9][9]; 
			
			for(int tCol = 0; tCol < 9; tCol++) {
				boolean[] tFirstPoint = getPoint(tRow, tCol);
				boolean[] tSecondPoint = pSecondWorkBoard.getPoint(tRow, tCol);
				
				for(int i=0; i<9; i++) {
					if(tFirstPoint[i] != tSecondPoint[i]) {
						tLine[tCol][i] = true;						
					}
				}				
			}

			printLine(tLine);

			tRowSpacer++;
			if(tRowSpacer == 3) {
				tRowSpacer = 0;
				printLineSeparator();
			}
			else {
				printSpacer();
			}
		}
	}

	private void printSpacer() {
		System.out.println("|                 |                 |                 |");
	}
	
	private void printLine(boolean[][] pLine) {		
		for(int q=0; q<7; q+=3) {

			String tStr = "| "; 
			
			for(int tCol = 0; tCol < 9; tCol++) {
				for(int w=0; w<3; w++) {
					if(pLine[tCol][q + w]) {
						tStr += q + w + 1;
					}
					else {
						tStr += " ";
					}
				}
			
				if(tCol%3 == 2) {					
					tStr += " | ";
				}
				else {
					tStr += "   ";
				}
			}
			
			System.out.println(tStr);
		}
	}

	private void printLineSeparator() {
		System.out.println("+-----------------+-----------------+-----------------+");
	}

	public boolean[] getPoint(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);

		return mValues[pRow][pCollumn]; 
	}


	public boolean isSingle(int pRow, int pCollumn) {
		int tCount = getCount(pRow, pCollumn);
		
		return tCount == 1;
	}


	public void remove(int pRow, int pCollumn, int pValue) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		assert(pValue >= 1);
		assert(pValue <= 9);
		
		int tValueIndex = pValue - 1;		
				
		mValues[pRow][pCollumn][tValueIndex] = false;
	}
	

	public int getCount(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);		
		
		int tCount = 0;
		for(int q = 0; q < 9; q++) {
			if(mValues[pRow][pCollumn][q]) {
				tCount++;
			}
		}

		return tCount;
	}

	public int getPointSingleValue(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		assert(isSingle(pRow, pCollumn));

		for(int q = 0; q < 9; q++) {
			if(mValues[pRow][pCollumn][q]) {
				return q + 1;
			}
		}
		
		assert(false);
		return 0;
	}



	public void removeAll(int pRow, int pCollumn) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);

		for(int i=0; i < 9; i++) {
			mValues[pRow][pCollumn][i] = false;
		}
	}
	
	public void removeAllBut(int pRow, int pCollumn, int pValue) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		assert(pValue >= 1);
		assert(pValue <= 9);
		
		int tIndex = pValue - 1;
		assert(mValues[pRow][pCollumn][tIndex]);

		for(int i=0; i < 9; i++) {
			if(i != tIndex) {
				mValues[pRow][pCollumn][i] = false;
			}
		}		
	}

	public boolean isValueAvalable(int pRow, int pCollumn, int pValue) {
		assert(pRow >= 0);
		assert(pRow <= 8);
		assert(pCollumn >= 0);
		assert(pCollumn <= 8);
		assert(pValue >= 1);
		assert(pValue <= 9);
		
		return mValues[pRow][pCollumn][pValue - 1];
	}
}
