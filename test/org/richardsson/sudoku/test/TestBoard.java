package org.richardsson.sudoku.test;

import org.richardsson.sudoku.Board;

import junit.framework.TestCase;

public class TestBoard extends TestCase {
	public void testCopyConstructor() {
		Board tB1 = new Board();
		tB1.setValue(2, 4, 7);
		tB1.setValue(3, 5, 9);
		tB1.setValue(7, 1, 1);
		
		Board tCopy = new Board(tB1);
		
		assertTrue(tCopy.hasValue(2, 4));
		assertEquals(7, tCopy.getValue(2, 4));

		assertTrue(tCopy.hasValue(3, 5));
		assertEquals(9, tCopy.getValue(3, 5));

		assertTrue(tCopy.hasValue(7, 1));
		assertEquals(1, tCopy.getValue(7, 1));

		assertFalse(tCopy.hasValue(1, 1));
		assertFalse(tCopy.hasValue(2, 2));
	}
	
	public void testPrintDiff() {
		Board tB1 = new Board();
		tB1.setValue(2, 4, 7);
		tB1.setValue(3, 5, 9);
		tB1.setValue(7, 1, 1);
		
		Board tB2 = new Board();
		
		tB2.printDiff(tB1);
	}
}
