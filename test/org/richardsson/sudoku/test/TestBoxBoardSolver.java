package org.richardsson.sudoku.test;

import org.richardsson.sudoku.Board;
import org.richardsson.sudoku.BoxBoardSolver;
import org.richardsson.sudoku.WorkBoard;

import junit.framework.TestCase;

public class TestBoxBoardSolver extends TestCase {

	public void testReduce() {
		Board tBoard = new Board(mSetup);
		WorkBoard tWorkBoard = new WorkBoard();
		BoxBoardSolver tBoxSolver = new BoxBoardSolver(0, 0);
		
		tBoxSolver.reduce(tBoard, tWorkBoard);
		
		assertEquals(6, tWorkBoard.getCount(0, 0));
		assertEquals(6, tWorkBoard.getCount(1, 1));
		assertEquals(6, tWorkBoard.getCount(2, 2));
		assertEquals(6, tWorkBoard.getCount(1, 0));
		assertEquals(6, tWorkBoard.getCount(2, 0));
		
		assertFalse(tWorkBoard.isValueAvalable(0, 0, 1));
		assertFalse(tWorkBoard.isValueAvalable(0, 0, 2));
		assertFalse(tWorkBoard.isValueAvalable(0, 0, 3));
		assertTrue(tWorkBoard.isValueAvalable(0, 0, 4));
	}
	
	private static String[] mSetup = {
		"1  " + "   " + "   ",
		"  2" + "   " + "   ",
		" 3 " + "   " + "   ",
		
		"   " + "   " + "   ",
		"   " + "   " + "   ",
		"   " + "   " + "   ",

		"   " + "   " + "   ",
		"   " + "   " + "   ",
		"   " + "   " + "   "};
}
