package org.richardsson.sudoku.test;

import org.richardsson.sudoku.Value;

import junit.framework.TestCase;

public class TestValue extends TestCase {
	
	public void testEquals() {
		{
			Value tV1 = new Value();
			Value tV2 = new Value();
			assertTrue(tV1.equals(tV2));
			assertTrue(tV2.equals(tV1));
		}

		{
			Value tV1 = new Value(1);
			Value tV2 = new Value();
			assertFalse(tV1.equals(tV2));
			assertFalse(tV2.equals(tV1));
		}

		{
			Value tV1 = new Value(3);
			Value tV2 = new Value(3);
			assertTrue(tV1.equals(tV2));
			assertTrue(tV2.equals(tV1));
		}

		{
			Value tV1 = new Value(4);
			Value tV2 = new Value(5);
			assertFalse(tV1.equals(tV2));
			assertFalse(tV2.equals(tV1));
		}
	}
}
