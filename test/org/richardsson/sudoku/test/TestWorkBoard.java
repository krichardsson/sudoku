package org.richardsson.sudoku.test;

import org.richardsson.sudoku.WorkBoard;

import junit.framework.TestCase;

public class TestWorkBoard extends TestCase {
	public void testPrint() {
		WorkBoard tWorkBoard = new WorkBoard();
		
		tWorkBoard.removeAll(1, 2);
		tWorkBoard.remove(2, 3, 1);
		assertEquals(false, tWorkBoard.isSingle(2, 3));

		tWorkBoard.print();
	}
	
	public void testRemove() {
		WorkBoard tWorkBoard = new WorkBoard();
		
		assertEquals(9, tWorkBoard.getCount(2, 3));
		
		assertTrue(tWorkBoard.isValueAvalable(2, 3, 9));
		tWorkBoard.remove(2, 3, 9);
		assertFalse(tWorkBoard.isValueAvalable(2, 3, 9));
		assertEquals(false, tWorkBoard.isSingle(2, 3));
		assertEquals(8, tWorkBoard.getCount(2, 3));
		
		assertTrue(tWorkBoard.isValueAvalable(2, 3, 8));
		tWorkBoard.remove(2, 3, 8);
		assertFalse(tWorkBoard.isValueAvalable(2, 3, 8));
		assertEquals(false, tWorkBoard.isSingle(2, 3));
		assertEquals(7, tWorkBoard.getCount(2, 3));

		assertTrue(tWorkBoard.isValueAvalable(2, 3, 7));
		tWorkBoard.remove(2, 3, 7);
		assertFalse(tWorkBoard.isValueAvalable(2, 3, 7));
		assertEquals(false, tWorkBoard.isSingle(2, 3));
		assertEquals(6, tWorkBoard.getCount(2, 3));

		assertTrue(tWorkBoard.isValueAvalable(2, 3, 6));
		tWorkBoard.remove(2, 3, 6);
		assertFalse(tWorkBoard.isValueAvalable(2, 3, 6));
		assertEquals(false, tWorkBoard.isSingle(2, 3));
		assertEquals(5, tWorkBoard.getCount(2, 3));

		assertTrue(tWorkBoard.isValueAvalable(2, 3, 5));
		tWorkBoard.remove(2, 3, 5);
		assertFalse(tWorkBoard.isValueAvalable(2, 3, 5));
		assertEquals(false, tWorkBoard.isSingle(2, 3));
		assertEquals(4, tWorkBoard.getCount(2, 3));

		assertTrue(tWorkBoard.isValueAvalable(2, 3, 4));
		tWorkBoard.remove(2, 3, 4);
		assertFalse(tWorkBoard.isValueAvalable(2, 3, 4));
		assertEquals(false, tWorkBoard.isSingle(2, 3));
		assertEquals(3, tWorkBoard.getCount(2, 3));

		assertTrue(tWorkBoard.isValueAvalable(2, 3, 3));
		tWorkBoard.remove(2, 3, 3);
		assertFalse(tWorkBoard.isValueAvalable(2, 3, 3));
		assertEquals(false, tWorkBoard.isSingle(2, 3));
		assertEquals(2, tWorkBoard.getCount(2, 3));

		assertTrue(tWorkBoard.isValueAvalable(2, 3, 2));
		tWorkBoard.remove(2, 3, 2);
		assertFalse(tWorkBoard.isValueAvalable(2, 3, 2));
		assertEquals(true, tWorkBoard.isSingle(2, 3));
		assertEquals(1, tWorkBoard.getCount(2, 3));

		assertTrue(tWorkBoard.isValueAvalable(2, 3, 1));
		tWorkBoard.remove(2, 3, 1);
		assertFalse(tWorkBoard.isValueAvalable(2, 3, 1));
		assertEquals(false, tWorkBoard.isSingle(2, 3));
		assertEquals(0, tWorkBoard.getCount(2, 3));
	}

	public void testRemoveAll() {
		WorkBoard tWorkBoard = new WorkBoard();
		
		tWorkBoard.removeAll(2, 3);
		assertEquals(false, tWorkBoard.isSingle(2, 3));
	}
	
	public void testRemoveAllBut() {
		WorkBoard tWorkBoard = new WorkBoard();
		
		tWorkBoard.removeAllBut(2, 3, 4);
		assertEquals(true, tWorkBoard.isSingle(2, 3));		
	}
	
	public void testCopyConstructor() {
		WorkBoard tFirstWorkBoard = new WorkBoard();
		
		tFirstWorkBoard.remove(2, 3, 1);
		tFirstWorkBoard.remove(2, 3, 2);
		tFirstWorkBoard.remove(2, 3, 3);
		tFirstWorkBoard.remove(2, 3, 4);
		tFirstWorkBoard.remove(2, 3, 5);
		tFirstWorkBoard.remove(2, 3, 7);
		tFirstWorkBoard.remove(2, 3, 8);
		tFirstWorkBoard.remove(2, 3, 9);
		
		WorkBoard tCopy = new WorkBoard(tFirstWorkBoard);
		assertEquals(true, tCopy.isSingle(2, 3));
		assertEquals(6, tCopy.getPointSingleValue(2, 3));		
	}
	
	public void testPrintDiff() {
		WorkBoard tFirsWorkBoard = new WorkBoard();
		
		WorkBoard tSecondWorkBoard = new WorkBoard();
		tSecondWorkBoard.removeAll(2, 3);		
		tSecondWorkBoard.remove(3, 4, 5);		
		
		tFirsWorkBoard.printDiff(tSecondWorkBoard);
	}
	
	public void testIsSingle() {
		WorkBoard tWb = new WorkBoard();
		
		tWb.remove(2, 3, 1);
		assertFalse(tWb.isSingle(2, 3));

		tWb.remove(2, 3, 2);
		assertFalse(tWb.isSingle(2, 3));

		tWb.remove(2, 3, 3);
		assertFalse(tWb.isSingle(2, 3));

		tWb.remove(2, 3, 4);
		assertFalse(tWb.isSingle(2, 3));

		tWb.remove(2, 3, 5);
		assertFalse(tWb.isSingle(2, 3));

		tWb.remove(2, 3, 6);
		assertFalse(tWb.isSingle(2, 3));

		tWb.remove(2, 3, 7);
		assertFalse(tWb.isSingle(2, 3));

		tWb.remove(2, 3, 8);
		assertTrue(tWb.isSingle(2, 3));

		tWb.remove(2, 3, 9);
		assertFalse(tWb.isSingle(2, 3));

		
	}
}
